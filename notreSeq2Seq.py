import os
import pathlib
import nltk
from nltk.tokenize import word_tokenize
import pickle
import re
import collections
from nltk.corpus import comtrans

Cornelldir ='C:\\Users\\mer_e\\Documents\\4Supinfo\\IA\\PythonTests\\CollectingData\\CornellSegmentsPairs.txt'
Artistsdir ='C:\\Users\\mer_e\\Documents\\4Supinfo\\IA\\PythonTests\\CollectingData\\artistQApairs.txt'
Genresdir ='C:\\Users\\mer_e\\Documents\\4Supinfo\\IA\\PythonTests\\CollectingData\\genresQApairs.txt'

def tokenize_sentences(filelocation):
    with open(filelocation, encoding='utf-8') as fp:  
        line = fp.readline()
        list_from = []
        list_to = []
        while line:
            sent1, sent2 = line.split('<SEP>')
            sent1 = word_tokenize(sent1)
            sent2 = word_tokenize(sent2)
            list_from.append(sent1)
            list_to.append(sent2)
    return list_from, list_to

def tokenize_merge_corpuses():
    # artists_from, artists_to = tokenize_sentences(Artistsdir)
    genres_from, genres_to = tokenize_sentences(Genresdir)
    # movieDi_from, movieDi_to = tokenize_sentences(Cornelldir)

    corpus_from = []
    # corpus_from.append(artists_from)
    corpus_from.append(genres_from)
    # corpus_from.append(movieDi_from)
    corpus_to = []
    # corpus_to.append(artists_to)
    corpus_to.append(genres_to)
    # corpus_to.append(movieDi_to)
    return corpus_from, corpus_to

def build_dataset(words, n_words):
    # create count - how many times each word appears
    count = [['GO', 0], ['PAD', 1], ['EOS', 2], ['UNK', 3]]
    count.extend(collections.Counter(words).most_common(n_words - 1))
    # create dictionary from count in which each word's value is its would be index.
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    # Data is a list of segments that have been translated to numbers. A list of lists of numbers
    data = list()
    unk_count = 0
    for word in words:
        index = dictionary.get(word, 0)
        if index == 0:
            unk_count += 1
        data.append(index)
    # A second dimension of count would be the unk_count - the amount of times that 0 appears in the number translation of
    # numbers. 
    count[0][1] = unk_count
    # create reversed dictionary
    reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return data, count, dictionary, reversed_dictionary

